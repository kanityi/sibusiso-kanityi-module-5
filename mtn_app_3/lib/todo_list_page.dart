import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class TodoPage extends StatefulWidget {
  const TodoPage({Key? key}) : super(key: key);

  @override
  State<TodoPage> createState() => _TodoPageState();
}

class _TodoPageState extends State<TodoPage> {
  @override
  Widget build(BuildContext context) {
    final titleController = TextEditingController();
    final descriptionController = TextEditingController();
    final dueDateController = TextEditingController();

    final todos = FirebaseFirestore.instance.collection('todos');

    _updateTodo(
        String todoId, String title, String description, String dueDate) {
      titleController.text = title;
      descriptionController.text = description;
      dueDateController.text = dueDate;

      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Center(child: Text('Add Todo item')),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: titleController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20)),
                        labelText: 'Title'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: descriptionController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20)),
                        labelText: 'Description'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: dueDateController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      labelText: "Due Date",
                    ),
                    keyboardType: TextInputType.datetime,
                  ),
                ),
              ],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Text('Cancel'),
            ),
            ElevatedButton(
              onPressed: () => {
                log(todoId),
                todos
                    .doc(todoId)
                    .update(
                      {
                        'title': titleController.text,
                        'description': descriptionController.text,
                        'due_date': dueDateController.text,
                      },
                    )
                    .then((value) => log("Todo Updated"))
                    .catchError(
                      (error) => log(
                        "Failed to update todo: $error",
                      ),
                    ),
                Navigator.pop(context, 'Save')
              },
              child: const Text('Save'),
            ),
          ],
        ),
      );
    }

    _deleteUser(String todoId) {
      log(todoId);
      todos
          .doc(todoId)
          .delete()
          .then((value) => log("Todo Deleted"))
          .catchError((error) => log("Failed to delete todo: $error"));
    }

    Future<void> _addTodo() async {
      final title = titleController.text;
      final description = descriptionController.text;
      final dueDate = dueDateController.text;

      final todosRef = FirebaseFirestore.instance.collection('todos').doc();

      todosRef
          .set(
            {
              'title': title,
              'description': description,
              'due_date': dueDate,
              'id': todosRef.id
            },
          )
          .then((value) => log('Todo added successful'))
          .catchError((err) => log(err));

      Navigator.pop(context);
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Todo App'),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: todos.snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return const Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }

          return ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data()! as Map<String, dynamic>;
              return Card(
                child: Column(
                  children: [
                    ListTile(
                      title: Text(data['title']),
                      subtitle: Text(
                        '${data['description']} is due ${data['due_date']}',
                      ),
                    ),
                    ButtonTheme(
                      child: ButtonBar(
                        children: [
                          TextButton(
                            onPressed: () => _updateTodo(
                              data['id'],
                              data['title'],
                              data['description'],
                              data['due_date'],
                            ),
                            child: const Icon(
                              Icons.edit,
                            ),
                          ),
                          TextButton(
                            onPressed: () => _deleteUser(data['id']),
                            child: const Icon(
                              Icons.delete,
                              color: Colors.red,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            }).toList(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: const Center(child: Text('Add Todo item')),
                content: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextField(
                          controller: titleController,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              labelText: 'Title'),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextField(
                          controller: descriptionController,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              labelText: 'Description'),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextField(
                          controller: dueDateController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            labelText: "Due Date",
                          ),
                          keyboardType: TextInputType.datetime,
                        ),
                      ),
                    ],
                  ),
                ),
                actions: [
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: const Text('Cancel'),
                  ),
                  ElevatedButton(
                    onPressed: () => _addTodo(),
                    child: const Text('Add'),
                  ),
                ],
              );
            }),
        child: const Icon(Icons.add),
      ),
    );
  }
}
